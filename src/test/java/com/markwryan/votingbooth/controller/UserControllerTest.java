package com.markwryan.votingbooth.controller;

import com.markwryan.votingbooth.VotingBoothApplication;
import com.markwryan.votingbooth.dto.UserDto;
import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.repository.UserRepository;
import com.markwryan.votingbooth.service.VotingBoothUserDetailsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by mark on 8/9/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:application-test.properties")
@ContextConfiguration(classes = {VotingBoothApplication.class})
@WebAppConfiguration
public class UserControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingBoothUserDetailsService userDetailsService;

    @Autowired
    private UserController controller;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    /**
     * Return a new User data transfer object in the model to transfer the form responses
     *
     * @throws Exception
     */
    @Test
    public void getRegistrationPage_AddsUserDtoToModel() throws Exception {
        mvc.perform(get("/register"))
                .andExpect(model().attribute("user", new UserDto()));
    }

    /**
     * Return the expected view
     * @throws Exception
     */
    @Test
    public void getRegistrationPage_ReturnsExpectedViewName() throws Exception {
        mvc.perform(get("/register"))
                .andExpect(view().name("register"));
    }

    /**
     * Correctly take in the UserDto and correctly save to the database.
     */
    @Test
    public void registerUser_AddsValidUser() {
        final String FIRST = "Test";
        final String LAST = "User";
        final String EMAIL = "testingNewUser@test.com";
        final String PASSWORD = "Password1!";

        UserDto userDto = new UserDto();
        userDto.setFirstName(FIRST);
        userDto.setLastName(LAST);
        userDto.setEmail(EMAIL);
        userDto.setPassword(PASSWORD);
        userDto.setConfirmPassword(PASSWORD);

        User user = new User();
        user.setFirstName(FIRST);
        user.setLastName(LAST);
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);

        BindingResult bindingResult = mock(BindingResult.class);

        controller.registerUser(userDto, bindingResult);

        User dbUser = userRepository.findByEmail(userDto.getEmail());

        assertEquals(dbUser.getFirstName(), user.getFirstName());
        assertEquals(dbUser.getLastName(), user.getLastName());
        assertEquals(dbUser.getEmail(), user.getEmail());
        //Password should have been encrypted on save.
        assertNotEquals(dbUser.getPassword(), user.getPassword());
    }

    /**
     * If a user exists with the same email, we should not overwrite it
     */
    @Test
    public void registerUser_DoesNotAddExistingUser() {
        final String EMAIL = "existingTestUser@test.com";

        UserDto userDto = new UserDto();
        userDto.setFirstName("myNew");
        userDto.setLastName("user");
        userDto.setEmail(EMAIL);
        userDto.setPassword("newPassword");
        userDto.setConfirmPassword("newPassword");

        //User is unique based on email, so other entries do not matter
        User existingUser = new User();
        existingUser.setFirstName("mark");
        existingUser.setLastName("test");
        existingUser.setEmail(EMAIL);
        existingUser.setPassword("SomeOtherPassword");
        //PRE-SAVE Existing User
        userDetailsService.save(existingUser);

        BindingResult bindingResult = mock(BindingResult.class);
        controller.registerUser(userDto, bindingResult);

        User dbUser = userRepository.findByEmail(userDto.getEmail());
        assertEquals(dbUser.getFirstName(), existingUser.getFirstName());
        assertEquals(dbUser.getLastName(), existingUser.getLastName());
        assertEquals(dbUser.getEmail(), existingUser.getEmail());
        assertEquals(dbUser.getPassword(), existingUser.getPassword());
    }

    /**
     * Don't register user if the password entries don't match
     */
    @Test
    public void registerUser_ChecksForPasswordsToMatch() {
        final String EMAIL = "existingTestUser@test.com";

        UserDto userDto = new UserDto();
        userDto.setFirstName("myNew");
        userDto.setLastName("user");
        userDto.setEmail(EMAIL);
        userDto.setPassword("newPassword");
        userDto.setConfirmPassword("someOtherPassword");

        BindingResult bindingResult = mock(BindingResult.class);
        controller.registerUser(userDto, bindingResult);

        User dbUser = userRepository.findByEmail(userDto.getEmail());
        assertNull(dbUser);
    }
}
