package com.markwryan.votingbooth.controller;

import com.markwryan.votingbooth.dto.UserDto;
import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.model.polls.Poll;
import com.markwryan.votingbooth.model.polls.response.PollResponse;
import com.markwryan.votingbooth.repository.PollRepository;
import com.markwryan.votingbooth.repository.PollResponseRepository;
import com.markwryan.votingbooth.service.VotingBoothUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for User-centric pages (profile, sign in, register)
 *
 * Created by mark on 8/7/17.
 */
@Controller
public class UserController {

    @Autowired
    private VotingBoothUserDetailsService userDetailsService;

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private PollResponseRepository pollResponseRepository;

    @RequestMapping(path = "/signin", method = RequestMethod.GET)
    public String getSignInPage() {
        return "signin";
    }


    @RequestMapping(path="/user/polls", method = RequestMethod.GET)
    public String getUserPolls(WebRequest request, Model model) {
        List<Poll> polls = pollRepository.findAll();
        model.addAttribute("polls", polls);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();
        User user = userDetailsService.getUserByEmail(email);
        model.addAttribute("user", user);
        List<PollResponse> pollResponses = pollResponseRepository.findAllByUser(user);

        List<Poll> incompletePolls = getIncompletePolls(polls, pollResponses);
        model.addAttribute("incompletePolls", incompletePolls);
        List<Poll> completedPolls = getCompletedPolls(pollResponses);
        model.addAttribute("compltetedPolls", completedPolls);

        return "userPolls";
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String getRegistrationPage(WebRequest request, Model model) {
        UserDto user = new UserDto();
        model.addAttribute("user", user);

        return "register";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") @Valid UserDto userDto, BindingResult result) {
        if(userDto.getPassword().matches(userDto.getConfirmPassword())) {
            createUserAccount(userDto);
        }
        else {
            result.reject("password", "Passwords do not match.");
        }
        return "register";
    }

    private void createUserAccount(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        //Password encrypted in service save call
        user.setPassword(userDto.getPassword());
        userDetailsService.save(user);

    }

    private List<Poll> getIncompletePolls(List<Poll> polls, List<PollResponse> pollResponses) {
        List<Poll> incompletePolls = new ArrayList<>();

        for(Poll poll : polls) {
            boolean hasResponse = false;
            for(PollResponse response : pollResponses) {
                if(poll == response.getPoll()) {
                    hasResponse = true;
                    break;
                }
            }
            if(!hasResponse) {
                incompletePolls.add(poll);
            }
        }
        return incompletePolls;
    }

    private List<Poll> getCompletedPolls(List<PollResponse> pollResponses) {
        List<Poll> completedPolls = new ArrayList<>();
        for(PollResponse pollResponse : pollResponses) {
            completedPolls.add(pollResponse.getPoll());
        }
        return completedPolls;
    }
}
