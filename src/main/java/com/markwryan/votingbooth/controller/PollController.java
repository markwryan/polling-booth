package com.markwryan.votingbooth.controller;

import com.markwryan.votingbooth.dto.*;
import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.model.polls.*;
import com.markwryan.votingbooth.model.polls.response.PollQuestionResponse;
import com.markwryan.votingbooth.model.polls.response.PollResponse;
import com.markwryan.votingbooth.repository.*;
import com.markwryan.votingbooth.service.VotingBoothUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Controller
public class PollController {
    @Autowired
    private VotingBoothUserDetailsService userDetailsService;

    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private PollQuestionChoiceRepository pollQuestionChoiceRepository;
    @Autowired
    private PollQuestionRepository pollQuestionRepository;
    @Autowired
    private PollResponseRepository pollResponseRepository;

    @RequestMapping(path = "/poll/{id}", method = RequestMethod.GET)
    public String LoadPoll(@PathVariable String id, Model model) {
        try {
            Long pollId = Long.parseLong(id);
            if (pollId > 0) {
                Poll poll = pollRepository.findOne(pollId);
                if (poll != null) {
                    model.addAttribute("pollDto", new PollDto());
                    model.addAttribute("poll", poll);

                    return "poll";
                }
            }
        } catch (NumberFormatException nfe) {
            //TODO: Redirect to specific error that poll does not exist
            return "redirect:/";
        }
        return "redirect:/";
    }

    @RequestMapping(path = "/poll/{id}", method = RequestMethod.POST)
    public String castVote(@PathVariable String id, @ModelAttribute PollDto pollDto) {
        try {
            Long pollId = Long.parseLong(id);
            if (pollId > 0) {
                Poll poll = pollRepository.findOne(pollId);
                if (poll != null) {
                    PollResponse response = new PollResponse();
                    response.setPoll(poll);

                    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                    String email = auth.getName();
                    User user = userDetailsService.getUserByEmail(email);
                    response.setUser(user);

                    Set<PollQuestionResponse> questionResponses = new HashSet<>();
                    for(String choice : pollDto.getChoices()) {
                        String[] splitChoice = choice.split(":");
                        Long questionId = Long.parseLong(splitChoice[0]);
                        Long choiceId = Long.parseLong((splitChoice[1]));
                        Long rank = Long.parseLong((splitChoice[2]));
                        PollQuestion question = pollQuestionRepository.findOne(questionId);
                        PollQuestionChoice questionChoice = pollQuestionChoiceRepository.findOne(choiceId);

                        PollQuestionResponse questionResponse = new PollQuestionResponse();
                        questionResponse.setPollQuestion(question);
                        questionResponse.setChoice(questionChoice);
                        questionResponse.setRank(rank);
                        questionResponses.add(questionResponse);
                    }

                    response.setPollQuestionResponses(questionResponses);
                    pollResponseRepository.save(response);
                }
            }
        } catch (NumberFormatException nfe) {
            //TODO: Redirect to specific error that poll does not exist
            return "redirect:/";
        }
        return "redirect:/";
    }

    /**
     * Route to quickly add a hardcoded poll
     * <p>
     * TODO: Obvious enhancement would be to put a form in front and administration flow to manage polls
     *
     * @return
     */
    @Transactional
    @RequestMapping("/poll/add")
    public String loadAddPollPage() {
        Poll poll = new Poll();
        poll.setName("Ballot");
        poll.setDescription("This is a example poll");

        PollQuestion question1 = new PollQuestion();
        question1.setDescription("Rank candidates in order of choice. Mark your favorite candidate as first choice and " +
                "then indicate your second choice and additional back-up choices in order of choice. You may rank as " +
                "many candidates as you want");
        question1.setTitle("For commander in cream and vice ice");
        question1.setPollQuestionType(PollQuestionType.RANKED);
        PollQuestionChoice choice1 = new PollQuestionChoice();
        choice1.setDescription("Reese WithoutASpoon (Democrat for C.I.C.) / Cherry Garcia (Democrat for Vice Ice)");
        PollQuestionChoice choice2 = new PollQuestionChoice();
        choice2.setDescription("Choco 'Chip' Dough (Republican for C.I.C.) / Carmela Coney (Republican for Vice Ice");
        PollQuestionChoice choice3 = new PollQuestionChoice();
        choice3.setDescription("Magic Browny (Independent for C.I.C.) / Phish Food (Independent for Vice Ice)");
        Set<PollQuestionChoice> choices1 = new HashSet<>();
        choices1.add(choice1);
        choices1.add(choice2);
        choices1.add(choice3);
        question1.setPollQuestionChoices(choices1);

        PollQuestion question2 = new PollQuestion();
        question2.setPollQuestionType(PollQuestionType.SINGLE);
        question2.setTitle("For chief dairy queen");
        question2.setDescription("Shall Justice Mint C. Chip of the Supreme Court of the State of Ice Cream be retained" +
                "in office for another term");
        PollQuestionChoice choice4 = new PollQuestionChoice();
        choice4.setDescription("YES");
        PollQuestionChoice choice5 = new PollQuestionChoice();
        choice5.setDescription("NO");
        HashSet<PollQuestionChoice> choices2 = new HashSet<>();
        choices2.add(choice4);
        choices2.add(choice5);
        question2.setPollQuestionChoices(choices2);

        PollQuestion question3 = new PollQuestion();
        question3.setPollQuestionType(PollQuestionType.MULTIPLE);
        question3.setMaxSelections(2);
        question3.setTitle("For State Rep. District M&M");
        question3.setDescription("(Vote for Two)");
        PollQuestionChoice choice6 = new PollQuestionChoice();
        choice6.setDescription("P. Nut Butter (Republican)");
        PollQuestionChoice choice7 = new PollQuestionChoice();
        choice7.setDescription("Marsh Mallow (Democrat)");
        PollQuestionChoice choice8 = new PollQuestionChoice();
        choice8.setDescription("Cream C Kol (Independent)");
        HashSet<PollQuestionChoice> choices3 = new HashSet<>();
        choices3.add(choice6);
        choices3.add(choice7);
        choices3.add(choice8);
        question3.setPollQuestionChoices(choices3);

        PollQuestion question4 = new PollQuestion();
        question4.setPollQuestionType(PollQuestionType.SINGLE);
        question4.setTitle("Ballot Issue - Constitutional Initiative no. 116");
        question4.setDescription("Make Vanilla (over Chocolate) the offical best flavor");
        PollQuestionChoice choice9 = new PollQuestionChoice();
        choice9.setDescription("Yes on CI - 116 (for Vanilla)");
        PollQuestionChoice choice10 = new PollQuestionChoice();
        choice10.setDescription("No on CI - 116 (No on Vanilla)");
        HashSet<PollQuestionChoice> choices4 = new HashSet<>();
        choices4.add(choice9);
        choices4.add(choice10);
        question4.setPollQuestionChoices(choices4);

        Set<PollQuestion> questions = new HashSet<>();
        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);

        poll.setPollQuestions(questions);
        pollRepository.save(poll);
        return "redirect:/user/polls";
    }

    @RequestMapping("/poll/get")
    public String getPoll() {
        Iterable<Poll> polls = pollRepository.findAll();
        Poll poll = polls.iterator().next();

        return poll.getDescription();
    }
}
