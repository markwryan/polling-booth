package com.markwryan.votingbooth.repository;

import com.markwryan.votingbooth.model.polls.Poll;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PollRepository extends CrudRepository<Poll, Long> {
    List<Poll> findAll();
}
