package com.markwryan.votingbooth.repository;

import com.markwryan.votingbooth.model.polls.PollQuestion;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mark on 8/11/17.
 */
public interface PollQuestionRepository extends CrudRepository<PollQuestion, Long> {
}
