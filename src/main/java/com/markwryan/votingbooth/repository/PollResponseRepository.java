package com.markwryan.votingbooth.repository;

import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.model.polls.response.PollResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by mark on 8/9/17.
 */
public interface PollResponseRepository extends CrudRepository<PollResponse, Long> {
    Iterable<PollResponse> findAll(Sort sort);

    Page<PollResponse> findAll(Pageable pageable);

    List<PollResponse> findAllByUser(User user);
}
