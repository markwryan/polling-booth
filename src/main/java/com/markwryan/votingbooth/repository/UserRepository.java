package com.markwryan.votingbooth.repository;

import com.markwryan.votingbooth.model.account.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mark on 8/7/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}
