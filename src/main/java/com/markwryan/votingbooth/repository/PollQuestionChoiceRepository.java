package com.markwryan.votingbooth.repository;

import com.markwryan.votingbooth.model.polls.PollQuestionChoice;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mark on 8/11/17.
 */
public interface PollQuestionChoiceRepository extends CrudRepository<PollQuestionChoice, Long> {
}
