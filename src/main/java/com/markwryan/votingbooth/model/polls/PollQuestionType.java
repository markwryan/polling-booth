package com.markwryan.votingbooth.model.polls;

/**
 * Created by mark on 8/11/17.
 */
public enum PollQuestionType {
    SINGLE, MULTIPLE, RANKED
}
