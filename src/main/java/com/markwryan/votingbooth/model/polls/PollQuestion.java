package com.markwryan.votingbooth.model.polls;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "POLL_QUESTION")
public class PollQuestion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POLL_QUESTION_ID")
    protected long pollQuestionId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "MAX_SELECTIONS")
    private Integer maxSelections;

    @Column(name = "SORT_ORDER")
    private Long sortOrder;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<PollQuestionChoice> pollQuestionChoices;

    @Column(name = "QUESTION_TYPE")
    private PollQuestionType pollQuestionType;

    public PollQuestion() { }

    public long getPollQuestionId() {
        return pollQuestionId;
    }

    public void setPollQuestionId(long pollQuestionId) {
        this.pollQuestionId = pollQuestionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortOrder() {
        return sortOrder;
    }

    public Integer getMaxSelections() {
        return maxSelections;
    }

    public void setMaxSelections(Integer maxSelections) {
        this.maxSelections = maxSelections;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Set<PollQuestionChoice> getPollQuestionChoices() {
        return pollQuestionChoices;
    }

    public void setPollQuestionChoices(Set<PollQuestionChoice> pollQuestionChoices) {
        this.pollQuestionChoices = pollQuestionChoices;
    }

    public PollQuestionType getPollQuestionType() {
        return pollQuestionType;
    }

    public void setPollQuestionType(PollQuestionType pollQuestionType) {
        this.pollQuestionType = pollQuestionType;
    }
}
