package com.markwryan.votingbooth.model.polls.response;

import com.markwryan.votingbooth.model.polls.PollQuestion;
import com.markwryan.votingbooth.model.polls.PollQuestionChoice;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mark on 8/9/17.
 */
@Entity
@Table(name = "POLL_QUESTION_RESPONSE")
public class PollQuestionResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POLL_QUESTION_RESPONSE_ID")
    private long pollQuestionResponseId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_POLL_QUESTION_ID")
    private PollQuestion pollQuestion;

    @ManyToOne
    private PollQuestionChoice choice;

    @Column(name = "RANK")
    private Long rank;

    public long getId() {
        return pollQuestionResponseId;
    }

    public void setId(long pollQuestionResponseId) {
        this.pollQuestionResponseId = pollQuestionResponseId;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public PollQuestion getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(PollQuestion pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public PollQuestionChoice getChoice() {
        return choice;
    }

    public void setChoice(PollQuestionChoice choice) {
        this.choice = choice;
    }
}