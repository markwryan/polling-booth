package com.markwryan.votingbooth.model.polls.response;

import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.model.polls.Poll;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by mark on 8/9/17.
 */
@Entity
@Table(name = "POLL_RESPONSE")
public class PollResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<PollQuestionResponse> pollQuestionResponses;

    @ManyToOne(cascade = CascadeType.DETACH)
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH)
    private Poll poll;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<PollQuestionResponse> getPollQuestionResponses() {
        return pollQuestionResponses;
    }

    public void setPollQuestionResponses(Set<PollQuestionResponse> pollQuestionResponses) {
        this.pollQuestionResponses = pollQuestionResponses;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
