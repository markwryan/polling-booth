package com.markwryan.votingbooth.model.polls;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "POLL_QUESTION_CHOICE")
public class PollQuestionChoice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POLL_QUESTION_CHOICE_ID")
    protected long pollQuestionChoiceId;

    @Column(name = "DESCRIPTION")
    private String description;

    public PollQuestionChoice() {
    }

    public long getPollQuestionChoiceId() {
        return pollQuestionChoiceId;
    }

    public void setPollQuestionChoiceId(long pollQuestionChoiceId) {
        this.pollQuestionChoiceId = pollQuestionChoiceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
