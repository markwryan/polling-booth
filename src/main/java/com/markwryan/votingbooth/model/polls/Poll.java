package com.markwryan.votingbooth.model.polls;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "POLL")
public class Poll implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POLL_ID")
    protected long pollId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "POLL_DESCRIPTION")
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<PollQuestion> pollQuestions;

    public Poll() { }

    public long getId() {
        return pollId;
    }

    public void setId(long id) {
        this.pollId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<PollQuestion> getPollQuestions() { return pollQuestions; }

    public void setPollQuestions(Set<PollQuestion> pollQuestions) {
        this.pollQuestions = pollQuestions;
    }
}
