package com.markwryan.votingbooth.dto;

/**
 * Created by mark on 8/12/17.
 */
public class PollDto {
    String[] choices;

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }
}
