package com.markwryan.votingbooth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VotingBoothApplication {

    public static void main(String[] args) {
        SpringApplication.run(VotingBoothApplication.class, args);
    }

}
