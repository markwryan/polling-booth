package com.markwryan.votingbooth.service;

import com.markwryan.votingbooth.model.account.User;
import com.markwryan.votingbooth.repository.UserRepository;
import com.markwryan.votingbooth.security.VotingBoothUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Service uses to load and to save users that sign in and register within the application
 *
 * Created by mark on 8/7/17.
 */
@Service
public class VotingBoothUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Attempt to grab the user record for the given email. Return as a (subclasses) UserDetails object to pass off
     * to the built in user/pass validator.
     * @param email - provided email
     * @return UserDetails
     * @throws UsernameNotFoundException if user does not exist
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        email = email.toLowerCase().trim();
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return new VotingBoothUserPrincipal(user);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    /**
     * If user does not exist, create a new user record, encrypting the password.
     * @param user - User to be saved
     * @return persisted User
     */
    @Transactional
    public User save(User user) {
        if(userRepository.findByEmail(user.getEmail()) == null) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user = userRepository.save(user);
        }
        return user;
    }
}
