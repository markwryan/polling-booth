# Voting Booth

## Overview
Build a very simple voting system that allows a voter to fill out their ballot online. The voting system should allow for tallying/tabulation, reporting of the final results and keeping a log of all user activity.

## Technical Overview
* Java 8
* Spring Boot
* Maven
* H2 In-Memory DB (dev, would need to be swapped)
* JPA

## Running Locally

### Requirements

#### JDK 8
Download and install from the [Oracle website](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

#### Maven
Follow install instructions from [Apache](https://maven.apache.org/install.html)

### Start server with Maven

To Start the internal server:

```
cd into_root_directory
mvn spring-boot:run
```

Open [localhost:8080](http://localhost:8080)

To exit:
control-c

## Deploy



